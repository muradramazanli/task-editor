import { createSelector } from 'reselect'
const task = (state: { root: { task: Store.TaskState } }) => state.root.task
const user = (state: { root: { user: Store.UserState } }) => state.root.user
// Data Selectors
export const selectTask = createSelector(task, (slice) => slice)
export const selectUser = createSelector(user, (slice) => slice)
