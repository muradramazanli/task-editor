import React, { useEffect } from 'react'
import TaskForm from 'components/TaskForm'
import { useForm } from 'antd/lib/form/Form'
import { useDispatch, useSelector } from 'react-redux'
import { setLoadingState, taskCreateRequest } from 'store/task/actions'
import { selectTask, selectUser } from 'containers/selectors'
import { LoadingState } from 'types/enums'
import { mapErrors } from 'services/mapService'


const TaskFormContainer: React.FC = () => {
    const [form] = useForm()
    const task = useSelector(selectTask)
    const { loadingState, errors } = task

    useEffect(() => {
        if (loadingState === LoadingState.CreateFailure) {
            const fields = mapErrors(errors)
            form.setFields(fields)
            dispatch(setLoadingState(LoadingState.Unset))
        }
    }, [loadingState])

    const dispatch = useDispatch()

    const onFinish = (data: CreateForm) => {
        dispatch(taskCreateRequest(data))
    }

    return (
        <TaskForm form={form} onFinish={onFinish} loading={loadingState === LoadingState.Creating} />
    )
}

export default TaskFormContainer