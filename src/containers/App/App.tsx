import React, { lazy, useEffect } from 'react'
import routes from 'config/routes'
import { Route, Switch, Redirect } from 'react-router-dom'
import { useDispatch } from 'react-redux'
import { readUser } from 'store/user/actions'

const Home = lazy(() => import('pages/Home'))
const Create = lazy(() => import('pages/Create'))
const Login = lazy(() => import('pages/Login'))
const Task = lazy(() => import('pages/Task'))

const App = () => {
  const dispatch = useDispatch()

  useEffect(() => {
    dispatch(readUser())
  })

  return (
    <Switch>
      <Route path={routes.create.path} component={Create} />
      <Route path={routes.login.path} component={Login} />
      <Route path={routes.task.path} component={Task} />
      <Route path={routes.home.path} component={Home} />
      <Redirect to={routes.home.path} />
    </Switch>
  )
}

export default App
