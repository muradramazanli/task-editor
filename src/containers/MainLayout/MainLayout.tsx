import React from 'react'
import MainLayout from 'components/MainLayout'
import { useDispatch, useSelector } from 'react-redux'
import { selectUser } from 'containers/selectors'
import { LoadingState } from 'types/enums'
import { storeUser } from 'store/user/actions'
import { message } from 'antd'


const MainLayoutContainer: React.FC = ({ children }) => {
    const dispatch = useDispatch()
    const user = useSelector(selectUser)
    const { token } = user

    const logout = () => {
        dispatch(storeUser(''))
        message.warning('you logged out')
    }

    return (
        <MainLayout loggedin={token !=='' } logout={logout}>
            {children}
        </MainLayout>
    )
}

export default MainLayoutContainer
