
import { useForm } from 'antd/lib/form/Form'
import LoginForm from 'components/LoginForm'
import routes from 'config/routes'
import { selectUser } from 'containers/selectors'
import React, { useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { Redirect } from 'react-router'
import { mapErrors } from 'services/mapService'
import { login, setLoadingState } from 'store/user/actions'
import { LoadingState } from 'types/enums'


const LoginContainer: React.FC = () => {
    const [form] = useForm()
    const user = useSelector(selectUser)
    const { loadingState, errors } = user

    useEffect(() => {
        if (loadingState === LoadingState.LoadFailure) {
            
            const fields = mapErrors(errors)
            
            form.setFields(fields)
            dispatch(setLoadingState(LoadingState.Unset))
        }
    }, [loadingState])

    const dispatch = useDispatch()

    const onFinish = (data: LoginForm) => {
        dispatch(login(data))
    }

    return <LoginForm form={form} onFinish={onFinish} loading={loadingState === LoadingState.Loading} />
}

export default LoginContainer