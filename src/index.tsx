import React, { Suspense } from 'react'
import ReactDOM from 'react-dom'
import './index.css'
import App from './containers/App'
import reportWebVitals from './reportWebVitals'
import { Provider } from 'react-redux'
import configureStore, { history } from 'store'
import { Spin } from 'antd'
import 'antd/dist/antd.css'
import 'style/main.css'
import { ConnectedRouter } from 'connected-react-router'

const store = configureStore(history)

ReactDOM.render(
  <React.StrictMode>
    <Provider store={store}>
      <Suspense fallback={<Spin tip="Loading..." />}>
        <ConnectedRouter history={history} noInitialPop>
          <App />
        </ConnectedRouter>
      </Suspense>
    </Provider>
  </React.StrictMode>,
  document.getElementById('root')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
