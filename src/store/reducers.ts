/**
 * Combine all reducers in this file and export the combined reducers.
 */

import { combineReducers } from 'redux'

import task from 'store/task/reducer'
import user from 'store/user/reducer'

import { STATE_KEY as TASK_KEY } from 'store/task/constants'
import { STATE_KEY as USER_KEY } from 'store/user/constants'

const rootReducer = combineReducers({
  [TASK_KEY]: task,
  [USER_KEY]: user,
})

export default rootReducer
