import { all } from 'redux-saga/effects'
import task from 'store/task/sagas'
import user from 'store/user/sagas'

export default function* rootSaga() {
  yield all([
    task(),
    user()
  ])
}
