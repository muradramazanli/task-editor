import { LoadingState } from 'types/enums'
import * as K from './constants'
import { TaskActionTypes } from './types'

export function taskRequest(): TaskActionTypes {
  return {
    type: K.FETCH_REQUEST,
    payload: null,
  }
}

export function taskParamsStore(params: Store.TasKRequestParams): TaskActionTypes {
  return {
    type: K.PAGE_REQUEST,
    payload: params,
  }
}
export function setRequestParams(params: Store.TasKRequestParams): TaskActionTypes {
  return {
    type: K.SET_REQUEST_PARAMS,
    payload: params,
  }
}

export function taskStore(tasks: Store.Task[]): TaskActionTypes {
  return {
    type: K.SET_TASKS,
    payload: tasks,
  }
}

export function taskCountStore(count: number): TaskActionTypes {
  return {
    type: K.SET_TASK_COUNT,
    payload: count,
  }
}

export function taskCountIncrement(): TaskActionTypes {
  return {
    type: K.SET_INCREMENT_TASK_COUNT,
    payload: null,
  }
}

export function taskCreateRequest(data: CreateForm): TaskActionTypes {
  return {
    type: K.CREATE_TASK_REQUEST,
    payload: data,
  }
}

export function taskEditRequest(data: UpdateForm): TaskActionTypes {
  return {
    type: K.EDIT_TASK_REQUEST,
    payload: data,
  }
}

export function taskEditStore(data: UpdateForm): TaskActionTypes {
  return {
    type: K.EDIT_TASK_STORE,
    payload: data,
  }
}

export function setErrors(data: Object): TaskActionTypes {
  return {
    type: K.SET_ERRORS,
    payload: data,
  }
}


export function setLoadingState(state: LoadingState): TaskActionTypes {
  return {
    type: K.SET_LOADING_STATE,
    payload: state,
  }
}