import produce from 'immer'
import { LoadingState, OrderByField, OrderDirection } from 'types/enums'
import * as K from './constants'
import { TaskActionTypes } from './types'

export const initialState: Store.TaskState = {
  tasks: [],
  loadingState: LoadingState.Unset,
  errors: {},
  page: 1,
  taskCount: 0,
  orderBy: OrderByField.Username,
  orderDirection: OrderDirection.Asc
}

const reducer = (state = initialState, action: TaskActionTypes): Store.TaskState => {

  switch (action.type) {
    case K.SET_TASKS:
      return produce(state, (draft) => {
        const data = action.payload as Store.Task[]
        draft.tasks = data
        draft.loadingState = LoadingState.Loaded
        return draft
      })

    case K.SET_ERRORS:
      return produce(state, (draft) => {
        const errors = action.payload as Json
        draft.errors = errors
        return draft
      })

    case K.SET_TASK_COUNT:
      return produce(state, (draft) => {
        const count = action.payload as number
        draft.taskCount = count
        return draft
      })

    case K.SET_INCREMENT_TASK_COUNT:
      return produce(state, (draft) => {
        draft.taskCount = draft.taskCount + 1
        return draft
      })

    case K.SET_REQUEST_PARAMS:
      return produce(state, (draft) => {
        const params = action.payload as Store.TasKRequestParams
        return {...draft, ...params}
      })

    case K.SET_LOADING_STATE:
      return produce(state, (draft) => {
        const state = action.payload as LoadingState
        draft.loadingState = state
        return draft
      })

    case K.EDIT_TASK_STORE:
      return produce(state, (draft) => {
        const form = action.payload as UpdateForm

        const index = state.tasks.findIndex((task) => task.id === form.id)
        if (index !== -1) {
          const task = state.tasks[index]
          const updatedTask = { ...task, ...form }
          const tasks = [...state.tasks.slice(0, index), updatedTask, ...state.tasks.slice(index + 1)]
          draft.tasks = tasks
        }

        return draft
      })

    default:
      return state
  }
}

export default reducer
