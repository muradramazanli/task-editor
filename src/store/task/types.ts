import { Action } from 'redux'
import { LoadingState } from 'types/enums'
import * as K from './constants'

interface TaskRequestAction extends Action {
  type: typeof K.FETCH_REQUEST
  payload: null
}
interface TaskStoreAction extends Action {
  type: typeof K.SET_TASKS
  payload: Store.Task[]
}

interface CreateTaskRequestAction extends Action {
  type: typeof K.CREATE_TASK_REQUEST
  payload: CreateForm
}

interface EditTaskRequestAction extends Action {
  type: typeof K.EDIT_TASK_REQUEST
  payload: UpdateForm
}

interface SetLoadingStateAction extends Action {
  type: typeof K.SET_LOADING_STATE
  payload: LoadingState
}

interface SetRequestParamsAction extends Action {
  type: typeof K.SET_LOADING_STATE
  payload: Store.TasKRequestParams
}

interface SetTaskCountAction extends Action {
  type: typeof K.SET_TASK_COUNT
  payload: number
}
interface TaskCountIncrement extends Action {
  type: typeof K.SET_INCREMENT_TASK_COUNT
  payload: number
}

interface SetErrorAction extends Action {
  type: typeof K.SET_ERRORS
  payload: Object
}

export type TaskActionTypes =
  | TaskRequestAction
  | SetLoadingStateAction
  | TaskStoreAction
  | CreateTaskRequestAction
  | SetErrorAction
  | EditTaskRequestAction
  | SetTaskCountAction
  | TaskCountIncrement
  | SetRequestParamsAction