import { put, takeLatest, call, all, select } from 'redux-saga/effects'

import Debug from 'debug'

import * as K from './constants'
import * as actions from './actions'
import * as api from 'api/task'
import { LoadingState } from 'types/enums'
import { TaskActionTypes } from './types'
import { message } from 'antd'
import { push, } from 'connected-react-router'
import routes from 'config/routes'
import { storeUser } from 'store/user/actions'

const debug = Debug('Frontend')

const selectTaskState = (state: { root: { task: Store.TaskState } }) => state.root.task


export function* fetchTasks() {
  try {
    yield put(actions.setLoadingState(LoadingState.Loading))
    const { page, orderDirection, orderBy } = yield select(selectTaskState)
    const taksFeed: Api.TaskFeed = yield call(api.getTasks, page, orderBy, orderDirection)

    if (taksFeed.status !== 'ok') {
      throw new Error("ERROR");
    }

    //put feed information in data store
    yield put(actions.taskStore(taksFeed.message.tasks))
    yield put(actions.taskCountStore(parseInt(taksFeed.message.total_task_count)))

    yield put(actions.setLoadingState(LoadingState.Loaded))
  } catch (error) {
    debug('Error', error)
    debug('fetchTasks error', error)

    yield put(actions.setLoadingState(LoadingState.LoadFailure))
  }
}

export function* fetchPage(action: TaskActionTypes) {
  const params = action.payload as Store.TasKRequestParams

  yield put(actions.setRequestParams(params))
  yield fetchTasks()

}

export function* createTask(action: TaskActionTypes) {
  try {
    yield put(actions.setLoadingState(LoadingState.Creating))
    const data = action.payload as CreateForm
    const response: Api.CreateTaskFeed = yield call(api.createTask, data)

    if (response.status !== 'ok') {
      const { message: error } = response
      yield put(actions.setErrors(error))
      throw new Error("ERROR");
    }

    message.success('Task is created successfully');
    //put feed information in data store
    yield put(actions.taskCountIncrement())
    yield put(actions.setLoadingState(LoadingState.Created))
    yield put(push(routes.home.path))
  } catch (error) {
    debug('Error', error)
    debug('fetchTasks error', error)
    yield put(actions.setLoadingState(LoadingState.CreateFailure))
  }
}

export function* updateTask(action: TaskActionTypes) {
  try {
    yield put(actions.setLoadingState(LoadingState.Updating))
    const data = action.payload as UpdateForm
    const response: Api.UpdateTaskFeed = yield call(api.updateTask, data)

    if (response.status !== 'ok') {
      const { message: error } = response
      if (error.token) {
        yield put(push(routes.login.path))
        yield put(storeUser(''))
      }
      yield put(actions.setErrors(error))
      throw new Error("ERROR");
    }

    message.success('Task is updated successfully');
    yield put(actions.setLoadingState(LoadingState.Updated))
    yield put(actions.taskEditStore(data))
  } catch (error) {
    debug('Error', error)
    debug('fetchTasks error', error)
    yield put(actions.setLoadingState(LoadingState.UpdateFailure))
  }
}

function* watchTasks() {
  yield all([
    takeLatest(K.FETCH_REQUEST, fetchTasks),
    takeLatest(K.CREATE_TASK_REQUEST, createTask),
    takeLatest(K.EDIT_TASK_REQUEST, updateTask),
    takeLatest(K.PAGE_REQUEST, fetchPage),
  ])
}

export default watchTasks

