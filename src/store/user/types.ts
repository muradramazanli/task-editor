import { Action } from 'redux'
import { LoadingState } from 'types/enums'
import * as K from './constants'

interface LoginAction extends Action {
  type: typeof K.LOGIN
  payload: LoginForm
}

interface UserStoreAction extends Action {
  type: typeof K.STORE_USER
  payload: string
}

interface SetLoadingStateAction extends Action {
  type: typeof K.SET_LOADING_STATE
  payload: LoadingState
}

interface SetErrorAction extends Action {
  type: typeof K.SET_ERRORS
  payload: Object
}

interface ReadUserAction extends Action {
  type: typeof K.READ_USER
  payload: null
}


export type UserActionTypes =
  | LoginAction
  | UserStoreAction
  | SetLoadingStateAction
  | SetErrorAction
  | ReadUserAction