export const STATE_KEY = 'user'

export const LOGIN = `${STATE_KEY}/LOGIN`
export const STORE_USER = `${STATE_KEY}/STORE_USER`
export const READ_USER = `${STATE_KEY}/READ_USER`
export const SET_LOADING_STATE = `${STATE_KEY}/SET_LOADING_STATE`
export const SET_ERRORS = `${STATE_KEY}/SET_ERRORS`
