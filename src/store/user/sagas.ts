import { put, takeLatest, call, all } from 'redux-saga/effects'

import Debug from 'debug'

import * as K from './constants'
import * as actions from './actions'
import * as api from 'api/login'
import { LoadingState } from 'types/enums'
import { UserActionTypes } from './types'
import { push } from 'connected-react-router'
import routes from 'config/routes'
import { message } from 'antd'

const debug = Debug('Frontend')

export function* login(action: UserActionTypes) {
  try {
    yield put(actions.setLoadingState(LoadingState.Loading))
    const form = action.payload as LoginForm
    const response: Api.LoginFeed = yield call(api.login, form)

    if (response.status !== 'ok') {
      const error = response.message
      yield put(actions.setErrors(error))

      throw new Error("ERROR")
    }
    message.success('Logged in!');

    const { message: success } = response as Api.LoginFeed
    const { token } = success as Api.LoginSuccess

    //put feed information in data store
    yield put(actions.storeUser(token))
    yield put(push(routes.home.path))

    yield put(actions.setLoadingState(LoadingState.Loaded))
  } catch (error) {
    debug('Error', error)
    debug('login error', error)

    yield put(actions.setLoadingState(LoadingState.LoadFailure))
  }
}

function* watchTasks() {
  yield all([
    takeLatest(K.LOGIN, login),
  ])
}

export default watchTasks

