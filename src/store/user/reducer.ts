import produce from 'immer'
import { LoadingState } from 'types/enums'
import * as K from './constants'
import { UserActionTypes } from './types'

export const initialState: Store.UserState = {
  token: '',
  loadingState: LoadingState.Unset,
  errors: {}
}

const reducer = (state = initialState, action: UserActionTypes): Store.UserState => {
  switch (action.type) {
    case K.STORE_USER:
      return produce(state, (draft) => {
        const token = action.payload as string
        localStorage.setItem('siteToken', token);
        draft.token = token
        return draft
      })
    case K.READ_USER:
      return produce(state, (draft) => {
        const token = localStorage.getItem('siteToken') as string
        if (token) {
          draft.token = token
        }
        return draft
      })
    case K.SET_LOADING_STATE:
      return produce(state, (draft) => {
        const loadingState = action.payload as LoadingState
        draft.loadingState = loadingState
        return draft
      })
    case K.SET_ERRORS:
      return produce(state, (draft) => {
        const errors = action.payload as Json
        draft.errors = errors
        return draft
      })
    default:
      return state
  }
}

export default reducer
