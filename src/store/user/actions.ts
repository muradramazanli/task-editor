import { LoadingState } from 'types/enums'
import * as K from './constants'
import { UserActionTypes } from './types'

export function login(data: LoginForm): UserActionTypes {
  return {
    type: K.LOGIN,
    payload: data,
  }
}

export function storeUser(token: string): UserActionTypes {
  return {
    type: K.STORE_USER,
    payload: token,
  }
}

export function setLoadingState(state: LoadingState): UserActionTypes {
  return {
    type: K.SET_LOADING_STATE,
    payload: state,
  }
}
export function readUser(): UserActionTypes {
  return {
    type: K.READ_USER,
    payload: null,
  }
}

export function setErrors(data: Json): UserActionTypes {
  return {
    type: K.SET_ERRORS,
    payload: data,
  }
}