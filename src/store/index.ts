import { createStore, applyMiddleware, compose, combineReducers } from 'redux'
import { composeWithDevTools } from 'redux-devtools-extension'
import createSagaMiddleware from 'redux-saga'
import { createBrowserHistory } from 'history'
import RootReducer from './reducers'
import RootSaga from './saga'
import { enableMapSet, enableES5 } from 'immer'
import { connectRouter, routerMiddleware } from 'connected-react-router'


export const history = createBrowserHistory()

export default function configureStore(history: any) {
  enableMapSet()
  enableES5()

  const sagaMiddleware = createSagaMiddleware()

  const composeEnhancers = composeWithDevTools(applyMiddleware(routerMiddleware(history), sagaMiddleware))

  const reducers = combineReducers({
    router: connectRouter(history),
    root: RootReducer
  })

  const store = createStore(reducers, composeEnhancers)

  sagaMiddleware.run(RootSaga)

  return store
}
