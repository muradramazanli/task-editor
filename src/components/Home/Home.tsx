import React from 'react'
import { Button, Table } from 'antd'
import { ColumnsType } from 'antd/lib/table'
import { Link, generatePath } from 'react-router-dom'
import routes from 'config/routes'
import { TaskStatus } from 'types/enums'
import { SorterResult } from 'antd/lib/table/interface'

interface Props {
    data: Store.Task[]
    page: number
    total: number
    getPage: (page: number, orderBy: string, direction: string) => void
    loading: boolean
    loggedIn: boolean
}

const Home: React.FC<Props> = ({ data, page, total, getPage, loading, loggedIn }) => {

    const columns: ColumnsType<Store.Task> = [
        {
            title: 'Username',
            dataIndex: 'username',
            key: 'username',
            sorter: true,
            defaultSortOrder: 'ascend'

        },
        {
            title: 'Email',
            dataIndex: 'email',
            key: 'email',
            sorter: true,
        },
        {
            title: 'Text',
            dataIndex: 'text',
            key: 'text',
        },
        {
            title: 'Status',
            dataIndex: 'status',
            key: 'status',
            sorter: true,
            render: (status) => {
                switch (status) {
                    case TaskStatus.Done:
                        return 'Done'
                    case TaskStatus.DoneEdited:
                        return 'Done Edited by Admin'
                    case TaskStatus.NotDone:
                        return 'Not Done'
                    case TaskStatus.NotDoneEdited:
                        return 'Not Done Edited by Admin'
                }
            }
        },
        {
            title: 'Edit',
            dataIndex: 'id',
            key: 'id',
            render: (id) => {
                return <Button disabled={!loggedIn}><Link to={generatePath(routes.task.path, { id })}>Edit</Link></Button>
            }
        }
    ];

    return (
        <Table
            dataSource={data}
            columns={columns}
            sortDirections={['descend', 'ascend']}
            pagination={{ pageSize: 3, total, current: page, }}
            onChange={(pagination, filter, sorter) => {
                const sort = sorter as SorterResult<Store.Task>
                getPage(pagination.current as number, sort.field as string, sort.order as string)
            }}
            loading={loading}
        />
    )
}

export default Home