import React from 'react'
import { Button, Form, FormInstance, Input } from 'antd'

interface Props {
    form: FormInstance
    onFinish: (data: LoginForm) => void
    loading: boolean
}

const LoginForm: React.FC<Props> = ({ form, onFinish, loading }) => {
    return (
        <Form form={form} onFinish={onFinish}>
            <Form.Item name="username" label="username" rules={[{ required: true }]}>
                <Input />
            </Form.Item>
            <Form.Item name="password" label="password" rules={[{ required: true }]}>
                <Input type="password" />
            </Form.Item>
            <Form.Item>
                <Button type="primary" htmlType="submit" loading={loading} disabled={loading}>Submit</Button>
            </Form.Item>
        </Form>
    )
}

export default LoginForm