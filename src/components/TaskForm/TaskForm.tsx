import React from 'react'
import { Button, Form, FormInstance, Input } from 'antd'

interface Props {
    form: FormInstance
    onFinish: (data: CreateForm) => void
    loading: boolean
}

const TaskForm: React.FC<Props> = ({ form, onFinish, loading }) => {
    return (
        <Form form={form} onFinish={onFinish}>
            <Form.Item name="username" label="username">
                <Input />
            </Form.Item>
            <Form.Item name="email" label="email">
                <Input type="email" />
            </Form.Item>
            <Form.Item name="text" label="text">
                <Input.TextArea rows={4} />
            </Form.Item>
            <Form.Item>
                <Button type="primary" htmlType="submit" loading={loading} disabled={loading}>Submit</Button>
            </Form.Item>
        </Form>
    )
}

export default TaskForm