import React from 'react'
import { Button, Checkbox, Col, Form, FormInstance, Input, Radio, Row, Table } from 'antd'
import { TaskStatus } from 'types/enums'

interface Props {
    form: FormInstance
    onFinish: (data: UpdateForm) => void
    loading: boolean
    task: Store.Task
}

const TaskUpdateForm: React.FC<Props> = ({ form, onFinish, loading, task }) => {
    return (
        <>
            <Row>
                <Col span={3}>Username</Col>
                <Col span={21}>{task.username}</Col>
            </Row>
            <Row>
                <Col span={3}>Email</Col>
                <Col span={21}>{task.email}</Col>
            </Row>
            <Form form={form} onFinish={onFinish}>
                <Form.Item name="text" label="text">
                    <Input.TextArea rows={4} />
                </Form.Item>
                <Form.Item name="status" label="Status">
                    <Radio.Group buttonStyle="solid">
                        <Radio.Button value={TaskStatus.NotDone}>Not Done</Radio.Button>
                        <Radio.Button value={TaskStatus.Done}>Done</Radio.Button>
                    </Radio.Group>
                </Form.Item>
                <Form.Item>
                    <Button type="primary" htmlType="submit" loading={loading} disabled={loading}>Submit</Button>
                </Form.Item>
            </Form>
        </>
    )
}

export default TaskUpdateForm