import React from 'react'
import { Layout, Menu } from 'antd'
import { Content, Header } from 'antd/lib/layout/layout'
import { NavLink } from 'react-router-dom'
import routes from 'config/routes'

interface Props {
    loggedin: boolean
    logout: () => void
}

const MainLayout: React.FC<Props> = ({ children, loggedin, logout }) => {
    return (
        <Layout className="layout">
            <Header className="header">
                <Menu theme="dark" mode="horizontal" defaultSelectedKeys={['2']}>
                    <Menu.Item><NavLink to={routes.home.path}>Home</NavLink></Menu.Item>
                    <Menu.Item><NavLink to={routes.create.path}>Create Task</NavLink></Menu.Item>
                    {!loggedin && <Menu.Item><NavLink to={routes.login.path}>Login</NavLink></Menu.Item>}
                    {loggedin && <Menu.Item onClick={logout}>Logout</Menu.Item>}
                </Menu>
            </Header>
            <Content style={{ padding: '0 50px' }}>
                {children}
            </Content>
        </Layout>
    )
}

export default MainLayout