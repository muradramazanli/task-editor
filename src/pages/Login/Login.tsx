
import MainLayout from 'containers/MainLayout'
import LoginContainer from 'containers/Login'
import React from 'react'


const Login:React.FC = ()=>{
    return <MainLayout>
        <LoginContainer />
    </MainLayout>
}

export default Login