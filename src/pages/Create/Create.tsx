
import MainLayout from 'containers/MainLayout'
import TaskForm from 'containers/TaskForm'
import React from 'react'


const Create: React.FC = () => {

    return (
        <MainLayout>
            <TaskForm />
        </MainLayout>
    )
}

export default Create