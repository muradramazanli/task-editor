import MainLayout from 'containers/MainLayout'
import HomeComponent from 'components/Home'
import React, { useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { taskEditRequest } from 'store/task/actions'
import { selectTask, selectUser } from 'containers/selectors'
import { LoadingState, TaskStatus } from 'types/enums'
import { Redirect, useParams } from 'react-router'
import { useForm } from 'antd/lib/form/Form'
import TaskForm from 'components/TaskForm'
import { Store } from 'rc-field-form/lib/interface'
import TaskUpdateForm from 'components/TaskUpdateForm'
import routes from 'config/routes'

interface Params {
    id: string
}

const Tasks: React.FC = () => {
    const dispatch = useDispatch()
    const [form] = useForm()
    const taskStore = useSelector(selectTask)
    const user = useSelector(selectUser)
    const params = useParams<Params>()
    const id = parseInt(params.id)
    const { tasks, loadingState } = taskStore
    const { token } = user

    const task = tasks.find((task) => task.id === id)

    useEffect(() => {
        if (task) {

            const formTask: Store.Task = {
                ...task,
                status: task.status === TaskStatus.Done || task.status === TaskStatus.DoneEdited ? TaskStatus.Done : TaskStatus.NotDone
            }
            form.setFieldsValue(formTask)
        }
    }, [task])

    if (token === '') {
        return <Redirect to={routes.home.path} />
    }

    if (!task) {
        return null
    }

    const onFinish = (data: UpdateForm) => {
        data.id = task.id
        if (data.text !== task.text) {
            data.status += 1
        }
        dispatch(taskEditRequest(data))
    }

    return (
        <MainLayout>
            <TaskUpdateForm
                loading={loadingState === LoadingState.Updating}
                form={form}
                onFinish={onFinish}
                task={task}
            />
        </MainLayout>
    )
}

export default Tasks