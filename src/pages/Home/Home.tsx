import MainLayout from 'containers/MainLayout'
import HomeComponent from 'components/Home'
import React, { useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { taskParamsStore, taskRequest } from 'store/task/actions'
import { selectTask, selectUser } from 'containers/selectors'
import { LoadingState, OrderByField, OrderDirection } from 'types/enums'


const Home: React.FC = () => {
    const dispatch = useDispatch()
    const task = useSelector(selectTask)
    const user = useSelector(selectUser)

    const { tasks, page, taskCount, loadingState } = task
    const { token } = user

    useEffect(() => {
        dispatch(taskRequest())
    }, [])

    const getPage = (page: number, orderBy: string, direction: string) => {

        dispatch(taskParamsStore({ page, orderBy, orderDirection: direction === 'ascend' ? OrderDirection.Asc : OrderDirection.Desc }))
    }

    return (
        <MainLayout>
            <HomeComponent
                data={tasks}
                total={taskCount}
                page={page}
                getPage={getPage}
                loading={loadingState === LoadingState.Loading}
                loggedIn={token != ''}
            />
        </MainLayout>
    )
}

export default Home