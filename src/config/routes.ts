const routes: Routes = {
  home: {
    path: '/',
  },
  task: {
    path: '/task/:id',
  },
  create: {
    path: '/create',
  },
  login: {
    path: '/login',
  },
}

export default routes
