export const api = (baseUrl: string, developer: string) => {
  return {
    api: {
      getTasks: (page: number, orderBy: string, orderDirection: string) =>
        `${baseUrl}/?developer=${developer}&page=${page}&sort_field=${orderBy}&sort_direction=${orderDirection}`,
      createTask: () => `${baseUrl}/create?developer=${developer}`,
      login: () => `${baseUrl}/login?developer=${developer}`,
      update: (id: number) => `${baseUrl}/edit/${id}?developer=${developer}`,
    },
  }
}

export default api
