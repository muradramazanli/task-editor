import axios from 'axios'
import { config } from 'core/config'
import { FormMap } from 'services/mapService'
import { getApiError } from './common'

export const login = async (data: LoginForm): Promise<Api.LoginFeed | ApiError> => {

  const formData = FormMap(data)

  return axios
    .post(config.api.login(), formData)
    .then((response) => response.data)
    .catch((reason) => getApiError(reason))
}