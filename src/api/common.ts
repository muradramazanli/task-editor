import { AxiosError } from 'axios'

export function getApiError(error: AxiosError): ApiError {
  return {
    status: error.response?.data.status || 'error',
    error: error.response?.data.message || {},
    statusCode: error.response?.status as number
  }
}
