import axios from 'axios'
import { config } from 'core/config'
import { FormMap } from 'services/mapService'
import { getApiError } from './common'

export const getTasks = async (page: number, orderBy: string, orderDirection: string): Promise<Api.TaskFeed | ApiError> => {

  return axios
    .get(config.api.getTasks(page, orderBy, orderDirection))
    .then((response) => response.data)
    .catch((reason) => getApiError(reason))
}

export const createTask = async (data: CreateForm): Promise<Api.CreateTaskFeed | ApiError> => {

  const formData = FormMap(data)

  return axios
    .post(config.api.createTask(), formData)
    .then((response) => response.data)
    .catch((reason) => getApiError(reason))
}

export const updateTask = async (data: UpdateForm): Promise<Api.CreateTaskFeed | ApiError> => {
  const token = localStorage.getItem('siteToken') as string
  const formData = FormMap(data, token)

  return axios
    .post(config.api.update(data.id), formData)
    .then((response) => response.data)
    .catch((reason) => getApiError(reason))
}

