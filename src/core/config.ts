import Debug from 'debug'
import { api } from 'config/api'
import _routes from 'config/routes'
const debug = Debug('Frontend')

/* eslint-disable no-useless-escape */
const platform = 'APP'

const apiBaseUrl = process.env.REACT_APP_API_BASE_URL as string
const developer = process.env.REACT_APP_DEVELOPER as string

export const config = (function config() {
  debug('ApiBaseUrl', apiBaseUrl)

  return {
    api: api(apiBaseUrl, developer).api,
    apiBaseUrl,
    platform,
  }
})()

export const routes = (function routes() {
  return _routes
})()
