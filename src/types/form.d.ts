interface CreateForm {
    username: string
    email: string
    text: string
}

interface UpdateForm {
    id: number
    text: string
    status: TaskStatus
}

interface LoginForm {
    username: string
    password: string
}