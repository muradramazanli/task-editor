declare namespace Store {

    interface UserState {
        token: string
        loadingState: LoadingState
        errors: Json
    }
}