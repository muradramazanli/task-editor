declare namespace Store {

    interface TaskState {
        tasks: Task[]
        loadingState: LoadingState
        errors: Json
        page: number
        taskCount: number
        orderBy: OrderByField
        orderDirection: OrderDirection
    }

    interface Task {
        id: number,
        email: string
        text: string
        status: TaskStatus
        username: string
    }

    interface TasKRequestParams {
        page:number
        orderBy: OrderByField
        orderDirection: OrderDirection
    }
}