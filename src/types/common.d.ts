interface Route {
  path: string
}

interface Routes {
  [key: string]: Route
}

interface ApiError {
  status: Api.Status
  error: any
  statusCode: number
}

type Json = {
  [name: string]: any
}
