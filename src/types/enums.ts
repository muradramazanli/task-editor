export enum TaskStatus {
    Done = 10,
    DoneEdited = 11,
    NotDone = 0,
    NotDoneEdited = 1
}

export enum LoadingState {
    Unset = 0,
    Loading = 1,
    Loaded = 2,
    LoadFailure = 3,
    Updating = 4,
    Updated = 5,
    UpdateFailure = 6,
    Creating = 7,
    Created = 8,
    CreateFailure = 9,
}

export enum OrderByField {
    Username = 'username',
    Email = 'email',
    Status = 'status'
}

export enum OrderDirection {
    Asc = 'asc',
    Desc = 'desc'
}