declare namespace Api {

    interface LoginFeed extends Feed {
        message: LoginSuccess | Object
    }
    interface LoginSuccess {
        token: string
    }
}