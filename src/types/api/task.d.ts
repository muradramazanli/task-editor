declare namespace Api {
    type Status = 'error' | 'ok'

    interface Feed {
        status: Status
    }

    interface TaskFeed extends Feed {
        message: {
            tasks: Task[],
            total_task_count: string
        }
    }

    interface CreateTaskFeed extends Feed {
        message: Task | Object
    }

    interface UpdateTaskFeed extends Feed {
        message: {
            token: string
        }
    }

    interface Task {
        id: number,
        email: string
        text: string
        status: number
        username: string
    }
}