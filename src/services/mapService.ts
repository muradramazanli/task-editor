
export function FormMap(data: Json, token?: string) {
    const formData = new FormData()

    if (token) {
        formData.append('token', token)
    }

    Object.keys(data).forEach((key) => {
        formData.append(key, !!data[key] ? data[key] : '')
    })

    return formData
}

export function mapErrors(errors: Json) {
    return Object.keys(errors).map((field: string) => ({
        name: field,
        errors: [errors[field]]
    }))
}